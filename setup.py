from setuptools import find_packages, setup

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setup(
    name='rjpy',
    version='0.1',
    license='GPL-v3',
    maintainer='Sreenadh TC',
    maintainer_email='kesav.tc8@gmail.com',
    description='Random JSON Schema for Python RestFul APIs',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=requirements,
    extras_require={
        'codestyle': [
            'pycodestyle'
        ],
        'test': [
            'pytest',
            'coverage',
        ],
    },
)
